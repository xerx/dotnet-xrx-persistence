﻿namespace Xrx.Persistence.SQLiteNET.Abstractions
{
    public interface ISQLiteEntity
    {
        int PK { get; set; }
    }
}
