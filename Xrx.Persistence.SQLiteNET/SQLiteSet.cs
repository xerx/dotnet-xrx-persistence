﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xrx.Persistence.Abstractions;
using Xrx.Persistence.SQLiteNET.Abstractions;

namespace Xrx.Persistence.SQLiteNET
{
    public class SQLiteSet<TEntity> : IPersistentSet<TEntity> where TEntity : ISQLiteEntity, new()
    {
        protected string _databasePath;
        public async Task<PersistentOperationResult<bool>> InitializeAsync(string databasePath)
        {
            _databasePath = databasePath;
            SQLiteAsyncConnection conn = new SQLiteAsyncConnection(databasePath);
            PersistentOperationResult<bool> result = new PersistentOperationResult<bool>
            {
                Type = PersistentOperationResultType.Success,
                Data = true
            };
            try
            {
                await conn.CreateTableAsync<TEntity>();
            }
            catch (Exception ex)
            {
                result.Type = PersistentOperationResultType.Error;
                result.Exception = ex;
                result.Data = false;
            }
            return result;
        }

        #region CREATE
        public async Task<PersistentOperationResult<bool>> CreateAsync(TEntity entity)
        {
            SQLiteAsyncConnection conn = new SQLiteAsyncConnection(_databasePath);
            PersistentOperationResult<bool> result = await SingleRowAffectingOperation(entity, conn.InsertOrReplaceAsync);
            await conn.CloseAsync();
            return result;
        }

        public async Task<PersistentOperationResult<IEnumerable<bool>>> CreateAsync(IEnumerable<TEntity> entities)
        {
            SQLiteAsyncConnection conn = new SQLiteAsyncConnection(_databasePath);
            List<bool> results = new List<bool>();
            PersistentOperationResult<bool> result;
            foreach (TEntity entity in entities)
            {
                result = await SingleRowAffectingOperation(entity, conn.InsertOrReplaceAsync);
                results.Add(!result.ResultIsError);
            }
            await conn.CloseAsync();
            return new PersistentOperationResult<IEnumerable<bool>>
            {
                Type = results.Exists(r => r == false) ?
                            PersistentOperationResultType.Incomplete :
                            PersistentOperationResultType.Success,
                Data = results
            };
        }
        #endregion

        #region READ
        public async Task<PersistentOperationResult<IEnumerable<TEntity>>> ReadAsync()
        {
            PersistentOperationResult<IEnumerable<TEntity>> result = new PersistentOperationResult<IEnumerable<TEntity>>();
            SQLiteAsyncConnection conn = new SQLiteAsyncConnection(_databasePath);
            try
            {
                result.Data = await conn.Table<TEntity>().ToListAsync();
            }
            catch (Exception ex)
            {
                result.Exception = ex;
                result.Type = PersistentOperationResultType.Error;
            }
            await conn.CloseAsync();
            return result;
        }
        public async Task<PersistentOperationResult<TEntity>> ReadAsync(object id)
        {
            PersistentOperationResult<TEntity> result = new PersistentOperationResult<TEntity>();
            bool idParsed = int.TryParse(id.ToString(), out int intId);
            if (!idParsed)
            {
                result.Type = PersistentOperationResultType.Error;
                result.Message = "Id cannot be parsed to int";
                return result;
            }
            SQLiteAsyncConnection conn = new SQLiteAsyncConnection(_databasePath);
            try
            {
                List<TEntity> entities = await conn.Table<TEntity>().ToListAsync();
                TEntity entity = entities.FirstOrDefault(e => e.PK == intId);
                if (entity != null)
                {
                    result.Type = PersistentOperationResultType.Success;
                    result.Data = entity;
                }
                else
                {
                    result.Type = PersistentOperationResultType.NotFound;
                }
            }
            catch (Exception ex)
            {
                result.Exception = ex;
                result.Type = PersistentOperationResultType.Error;
            }
            await conn.CloseAsync();
            return result;
        }
        #endregion READ

        #region UPDATE
        public async Task<PersistentOperationResult<bool>> UpdateAsync(TEntity entity)
        {
            SQLiteAsyncConnection conn = new SQLiteAsyncConnection(_databasePath);
            PersistentOperationResult<bool> result = await SingleRowAffectingOperation(entity, conn.UpdateAsync);
            await conn.CloseAsync();
            return result;
        }
        public async Task<PersistentOperationResult<IEnumerable<bool>>> UpdateAsync(IEnumerable<TEntity> entities)
        {
            SQLiteAsyncConnection conn = new SQLiteAsyncConnection(_databasePath);
            List<bool> results = new List<bool>();
            PersistentOperationResult<bool> result;
            foreach (TEntity entity in entities)
            {
                result = await SingleRowAffectingOperation(entity, conn.UpdateAsync);
                results.Add(!result.ResultIsError);
            }
            await conn.CloseAsync();
            return new PersistentOperationResult<IEnumerable<bool>>
            {
                Type = results.Exists(res => res == false) ?
                            PersistentOperationResultType.Incomplete :
                            PersistentOperationResultType.Success,
                Data = results
            };
        }
        #endregion

        #region DELETE
        public async Task<PersistentOperationResult<bool>> DeleteAsync(TEntity entity)
        {
            SQLiteAsyncConnection conn = new SQLiteAsyncConnection(_databasePath);
            PersistentOperationResult<bool> result = await SingleRowAffectingOperation(entity, conn.DeleteAsync);
            await conn.CloseAsync();
            return result;
        }
        public async Task<PersistentOperationResult<bool>> DeleteAllAsync()
        {
            SQLiteAsyncConnection conn = new SQLiteAsyncConnection(_databasePath);
            PersistentOperationResult<bool> result = new PersistentOperationResult<bool>();
            try
            {
                int numRowsAffected = await conn.DeleteAllAsync<TEntity>();
                result.Type = numRowsAffected > 0 ? PersistentOperationResultType.Success : PersistentOperationResultType.Incomplete;
                if (numRowsAffected > 0)
                {
                    result.Type = PersistentOperationResultType.Success;
                    result.Data = true;
                }
                else
                {
                    result.Type = PersistentOperationResultType.Incomplete;
                }
            }
            catch (Exception ex)
            {
                result.Exception = ex;
                result.Type = PersistentOperationResultType.Error;
            }
            await conn.CloseAsync();
            return result;
        }
        #endregion


        protected virtual async Task<PersistentOperationResult<bool>> SingleRowAffectingOperation(TEntity entity, Func<object, Task<int>> operation)
        {
            PersistentOperationResult<bool> result = new PersistentOperationResult<bool>();
            try
            {
                int numRowsAffected = await operation(entity);
                if (numRowsAffected > 0)
                {
                    result.Type = PersistentOperationResultType.Success;
                    result.Data = true;
                }
                else
                {
                    result.Type = PersistentOperationResultType.Incomplete;
                }
            }
            catch (Exception ex)
            {
                result.Exception = ex;
                result.Type = PersistentOperationResultType.Error;
            }
            return result;
        }
        protected virtual async Task<PersistentOperationResult<bool>> MultipleRowsAffectingOperation(IEnumerable<TEntity> entities, Func<IEnumerable<TEntity>, bool, Task<int>> operation)
        {
            PersistentOperationResult<bool> result = new PersistentOperationResult<bool>();
            try
            {
                int numRowsAffected = await operation(entities, true);
                if (numRowsAffected > 0)
                {
                    result.Type = PersistentOperationResultType.Success;
                    result.Data = true;
                }
                else
                {
                    result.Type = PersistentOperationResultType.Incomplete;
                }
            }
            catch (Exception ex)
            {
                result.Exception = ex;
                result.Type = PersistentOperationResultType.Error;
            }
            return result;
        }
    }
}
