﻿using System;

namespace Xrx.Persistence
{
    public class PersistentOperationResult<TData>
    {
        public PersistentOperationResultType Type { get; set; } = PersistentOperationResultType.Success;
        public string Message { get; set; }
        public virtual TData Data { get; set; }
        public Exception Exception { get; set; }
        public virtual bool ResultIsError => Type != PersistentOperationResultType.Success;
    }
}
