﻿namespace Xrx.Persistence
{
    public enum PersistentOperationResultType
    {
        Success,
        NotFound,
        Error,
        Incomplete,
        Timeout,
        Exists
    }
}
