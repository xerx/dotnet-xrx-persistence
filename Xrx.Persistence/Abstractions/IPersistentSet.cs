﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Xrx.Persistence.Abstractions
{
    public interface IPersistentSet<TEntity>
    {
        Task<PersistentOperationResult<bool>> InitializeAsync(string databasePath);
        Task<PersistentOperationResult<bool>> CreateAsync(TEntity entity);
        Task<PersistentOperationResult<IEnumerable<bool>>> CreateAsync(IEnumerable<TEntity> entities);
        Task<PersistentOperationResult<IEnumerable<TEntity>>> ReadAsync();
        Task<PersistentOperationResult<TEntity>> ReadAsync(object id);
        Task<PersistentOperationResult<bool>> UpdateAsync(TEntity entity);
        Task<PersistentOperationResult<IEnumerable<bool>>> UpdateAsync(IEnumerable<TEntity> entities);
        Task<PersistentOperationResult<bool>> DeleteAsync(TEntity entity);
        Task<PersistentOperationResult<bool>> DeleteAllAsync();
    }
}
