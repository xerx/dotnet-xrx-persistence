﻿using System.Threading.Tasks;

namespace Xrx.Persistence.Abstractions
{
    public interface IPersistenceService
    {
        Task<bool> InitializeAsync();
    }
}
